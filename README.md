This is a very SIMPLE api to create GUIs in bukkit with ease.

Requisites: Java 8, Lombok, Bukkit API

How to implement w/ maven:
```
<dependency>
	<groupId>me.mario</groupId>
	<artifactId>guiapi</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```