package me.paul.guiapi;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.paul.guiapi.listener.GuiListener;

public class Main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(new GuiListener(), this);
	}
	
	@Override
	public void onDisable() {
	}

//	 @Override
//	public boolean onCommand(CommandSender sender, Command command,
//			String label, String[] args) {
//		 if(command.getName().equalsIgnoreCase("testgui")) {
//			 if(!(sender instanceof Player)) return true;
//			 
//			 Player player = (Player) sender;
//			 new TestGui().show(player);
//		 }
//		 return true;
//	}
	
}
