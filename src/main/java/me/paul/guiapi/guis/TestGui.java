package me.paul.guiapi.guis;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import me.paul.guiapi.GuiButton;
import me.paul.guiapi.GuiPage;
import me.paul.guiapi.util.ItemBuilder;

public class TestGui extends GuiPage {

	public TestGui() {
		super(1, ChatColor.BLUE + "Test Gui");
		
		GuiButton button = new GuiButton(ItemBuilder.of(Material.STONE).name(ChatColor.GOLD + "The Stone").build());
		button.setListener((event) -> {
			event.setCancelled(true);
			Player player =(Player) event.getWhoClicked();
			player.closeInventory();
			player.sendMessage(ChatColor.RED + "You hit the stone!");
		});
		
		GuiButton button2 = new GuiButton(ItemBuilder.of(Material.DIRT).name(ChatColor.BLUE + "The Dirt").build());
		button2.setListener((event) -> {
			event.setCancelled(true);
			Player player =(Player) event.getWhoClicked();
			player.closeInventory();
			player.sendMessage(ChatColor.GREEN + "You hit the dirt!");
		});
		
		addButton(button);
		addButton(button2);
	}
}
