package me.paul.guiapi;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class GuiPage {
	
	public static Map<GuiPage, Set<UUID>> guipages = new LinkedHashMap<>();
	
	private Inventory inv;
	private Map<Integer, GuiButton> buttons = new LinkedHashMap<>();
	
	/**
	 * Constructor for GUI
	 * @param height Height of the GUI
	 */
	public GuiPage(int height) {
		this(height, "GUI");
	}
	
	/**
	 * Constructor for GUI Page
	 * @param height Height of the GUI
	 * @param name Name of the GUI
	 */
	public GuiPage(int height, String name) {
		if(height <= 0) throw new IllegalArgumentException("Height cannot be less than or equal to 0");
		
		this.inv = Bukkit.createInventory(null, height * 9, name);
		
		guipages.put(this, new HashSet<>());
	}
	
	/**
	 * @return Size of the inventory
	 */
	public int getSize() {
		return inv.getSize();
	}
	
	/**
	 * @return Next available slot in the inventory, will return -1 if there is no available slot
	 */
	public int nextIndex() {
		for(int i = 0; i < getSize(); i++) {
			if(!buttons.containsKey(i)) return i;
		}
		return -1;
	}
	
	/**
	 * Adds button to the GUI
	 * @param button Button to be added
	 * @throws RuntimeException if Inventory is full
	 */
	public void addButton(GuiButton button) {
		int index = nextIndex();
		
		if(index == -1) throw new RuntimeException("Inventory cannot be full!");
		setButton(index, button);
	}
	
	/**
	 * Sets a button in the GUI at a certain slot
	 * @param position Slot where the button will be placed
	 * @param button Button to be added
	 * @throws IllegalArgumentException if the position is higher than the size of the inventory
	 */
	public void setButton(int position, GuiButton button) {
		if(position > getSize()) throw new IllegalArgumentException("Position cannot be bigger than the size!");
		buttons.put(position, button);
		button.setGui(this);
		update();
	}
	
	/**
	 * Sets all buttons to correct position
	 */
	public void update() {
		inv.clear();
		
		buttons.entrySet().forEach(entry -> {
			inv.setItem(entry.getKey(), entry.getValue().getItem());
		});
	}
	
	/**
	 * @param slot Slot of where the button *should* be located
	 * @return Button at the slot specified
	 */
	public GuiButton getButton(int slot) {
		int exists = buttons.keySet().stream().filter(i -> i == slot).findFirst().orElse(-1);
		
		if(exists != -1)
			return buttons.get(exists);
		
		return null;
	}
	
	/**
	 * Shows the player the GUI
	 * @param player Player who the GUI will be shown to
	 */
	public void show(Player player) {
		guipages.get(this).add(player.getUniqueId());
		player.openInventory(inv);
	}
	
	/**
	 * @param inv Inventory of GUI Page
	 * @return GuiPage based on inventory
	 */
	public static GuiPage get(Inventory inv) {
		return guipages.keySet().stream().filter(gui -> gui.getInv().equals(inv)).findAny().orElse(null);
	}
	
	/**
	 * @param player Player that may or may not be in the GUI
	 * @return true if player is currently in GUI
	 */
	public boolean hasPlayer(Player player) {
		return guipages.get(this).stream().filter(uuid -> uuid.equals(player.getUniqueId())).findAny().isPresent();
	}
	
}
