package me.paul.guiapi;

import org.bukkit.event.inventory.InventoryClickEvent;

public abstract class GuiClickEvent {
	
	public abstract void onClick(InventoryClickEvent event);
	
}
