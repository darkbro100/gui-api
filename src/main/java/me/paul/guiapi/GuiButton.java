package me.paul.guiapi;

import java.util.function.Consumer;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class GuiButton {
	
	private GuiPage gui;
	private GuiClickEvent listener;
	private ItemStack item;
	
	/**
	 * Constructor for the GuiButton
	 * @param item ItemStack icon for this GuiButton
	 */
	public GuiButton(ItemStack item) {
		this.item = item;
	}
	
	/**
	 * Sets the listener for this button
	 * @param consumer Consumer for the InventoryClickEvent
	 */
	public void setListener(Consumer<InventoryClickEvent> consumer) {
		this.listener = new GuiClickEvent() {
			@Override
			public void onClick(InventoryClickEvent event) {
				consumer.accept(event);
			}
		};
	}
	
}
