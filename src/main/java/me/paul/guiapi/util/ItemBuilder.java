package me.paul.guiapi.util;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {
	
	private ItemStack item;
	private ItemMeta meta;
	
	public ItemBuilder(Material material) {
		this.item = new ItemStack(material);
		this.meta = this.item.getItemMeta();
	}
	
	public static ItemBuilder of(Material material) {
		return new ItemBuilder(material);
	}
	
	public ItemBuilder amount(int amount) {
		this.item.setAmount(amount);
		return this;
	}
	
	public ItemBuilder name(String name) {
		this.meta.setDisplayName(name);
		this.item.setItemMeta(meta);
		
		return this;
	}
	
	public ItemBuilder lore(String... lore) {
		List<String> loreList = Arrays.asList(lore);
		this.meta.setLore(loreList);
		this.item.setItemMeta(meta);
		
		return this;
	}
	
	public ItemStack build() {
		return this.item;
	}
	
}
